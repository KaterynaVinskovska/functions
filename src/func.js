const getSum = (str1, str2) => {
  if (typeof str1 != 'string' || typeof str2 != 'string' || isNaN(+str1) || isNaN(+str2)) return false;

  return (+str1 + +str2).toString()
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const arr = listOfPosts.filter(item => item.author === authorName);
  const newArr = listOfPosts.filter(item => item.comments);
  let comArr = [];
  newArr.forEach(item => {
    item.comments.forEach(com => {
      comArr.push(com)
    })
  }) 
  comArr = comArr.filter(item => item.author === authorName);
  return  `Post:${arr.length},comments:${comArr.length}`;
};

const tickets=(people)=> {
  let ticket = 0;
  for (i = 0; i < people.length; i++){
    if (people[i] == 25){
      ticket += 25
    } else {
      ticket = ticket - people[i] + 25;
      if (ticket < 0) {
        return "NO";
      } 
      ticket += people[i];
    }
}
    if (ticket < 0){
    return "NO";
  } else {
    return "YES";
  }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
